<?php
use Moment\Moment;
class Model_Pegawai extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getAllPegawai()
    {
        $query = $this->db->query("SELECT * FROM tabel_pegawai ORDER BY nama ASC");
        return $query;
    }

    public function ambilpegawai($id)
    {
        $query = $this->db->query("SELECT * FROM tabel_pegawai WHERE id = '$id'");
        return $query;
    }

    public function ambilpegawai_nip($nip)
    {
        $query = $this->db->query("SELECT * FROM tabel_pegawai WHERE nip = '$nip'");
        return $query;
    }

    public function simpanpegawai($nip, $nama, $tempat_lahir, $tanggal_lahir, $email,$bidang, $no_hp, $tmt_sk)
    {
        $query = $this->db->query("INSERT INTO tabel_pegawai(`nip`,`nama`,`tempat_lahir`,`tanggal_lahir`,`email`, `bidang`, `no_hp`,`tmt_sk`) 
            VALUES ('$nip','$nama','$tempat_lahir','$tanggal_lahir','$email','$bidang','$no_hp','$tmt_sk')");

        if($query){
            $pegawai = $this->db->query("SELECT id, tmt_sk FROM tabel_pegawai WHERE nip = '$nip'")->result_array();
            $id = $pegawai[0]['id'];
            $tmt_sk = $pegawai[0]['tmt_sk'];
            $moment = new Moment($tmt_sk);
            $tanggal_pengingat = $moment->subtractMonths(3)->format('Y-m-d');
            $this->db->query("INSERT INTO tabel_reminder(`id_pegawai`,`tanggal_pengingat`,`status`) VALUES ('$id','$tanggal_pengingat',null) ");
        }
        return $pegawai;
    }

    public function hapusPegawai($id)
    {
        $query = $this->db->query("DELETE FROM tabel_pegawai WHERE id = '$id'");
        return $query;
    }

    public function ubahpegawai($id, $nip, $nama, $tempat_lahir, $tanggal_lahir, $email, $bidang, $no_hp, $tmt_sk)
    {
        $curr_tmt_sk = $this->db->query("SELECT tmt_sk FROM tabel_pegawai WHERE id = '$id'")->result_array()[0]['tmt_sk'];
        $new_tmt_sk = new Moment($tmt_sk);
        $new_tmt_sk = $new_tmt_sk->format('Y-m-d');
        
        $query = $this->db->query("UPDATE tabel_pegawai SET nip = '$nip', nama = '$nama', tempat_lahir = '$tempat_lahir',
            tanggal_lahir = '$tanggal_lahir', email = '$email', bidang = '$bidang', no_hp = '$no_hp', tmt_sk = '$tmt_sk'
            WHERE id = '$id'");

        $deleteReminder = $this->db->query("DELETE FROM tabel_reminder WHERE `id_pegawai` = '$id' AND `status` IS NULL ORDER BY id DESC LIMIT 1");        

        if($query && $curr_tmt_sk != $new_tmt_sk){
            $moment = new Moment($new_tmt_sk);
            $tanggal_pengingat = $moment->subtractMonths(3)->format('Y-m-d');
            $this->db->query("INSERT INTO tabel_reminder(`id_pegawai`,`tanggal_pengingat`,`status`) VALUES ('$id','$tanggal_pengingat',null) ");
        }

        return $query;
    }

    public function updateTelegramId($nip, $telegram_id)
    {   
        $query = $this->db->query("UPDATE tabel_pegawai SET telegram_id = '$telegram_id' WHERE nip = '$nip'");

        return $query;
    }

    public function countPegawai(){
        return $this->db->query("SELECT COUNT(*) as pegawai_count FROM tabel_pegawai")->row('pegawai_count');
    }

    public function countBidang(){
        return $this->db->query("SELECT COUNT(DISTINCT bidang) as bidang_count from tabel_pegawai")->row('bidang_count');
    }
}
