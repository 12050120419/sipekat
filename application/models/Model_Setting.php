<?php

class Model_Setting extends CI_Model{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getPesan(){

        $query = $this->db->query("SELECT pesan FROM tabel_setting WHERE id = 1")->row('pesan');

        return $query;
    }

    public function getJangkaWaktu(){

        $query = $this->db->query("SELECT jangka_waktu FROM tabel_setting WHERE id=1")->row('jangka_waktu');

        return $query;
    }

    public function updateSetting($pesan, $jangka_waktu){
        $query = $this->db->query("UPDATE tabel_setting SET pesan = '$pesan', jangka_waktu = '$jangka_waktu'  WHERE id = 1");
        return $query;
    }

}