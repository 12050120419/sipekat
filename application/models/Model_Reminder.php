<?php
use Moment\Moment;

class Model_Reminder extends CI_Model{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getAllReminders(){

        $query = $this->db->query("SELECT tabel_reminder.id, tabel_reminder.id_pegawai, tabel_reminder.tanggal_pengingat, tabel_reminder.status, tabel_pegawai.nip, tabel_pegawai.nama, tabel_pegawai.tmt_sk, tabel_pegawai.no_hp FROM tabel_reminder INNER JOIN tabel_pegawai ON tabel_reminder.id_pegawai=tabel_pegawai.id")->result_array();

        return $query;
    }

    public function getActiveReminders(){

        $start_date = new Moment();
        $end_date = new Moment();

        $jangka_waktu = $this->db->query("SELECT jangka_waktu FROM tabel_setting WHERE id=1")->row('jangka_waktu');

        $start_date = $start_date->format('Y-m-d');
        $end_date = $end_date->addMonths($jangka_waktu)->format('Y-m-d');

        $query = $this->db->query("SELECT tabel_reminder.id, tabel_reminder.id_pegawai, tabel_reminder.tanggal_pengingat, tabel_reminder.status, tabel_pegawai.nip, tabel_pegawai.nama, tabel_pegawai.tmt_sk, tabel_pegawai.no_hp, tabel_pegawai.telegram_id FROM tabel_reminder INNER JOIN tabel_pegawai ON tabel_reminder.id_pegawai=tabel_pegawai.id WHERE tmt_sk BETWEEN '$start_date' AND '$end_date' AND tabel_reminder.status IS NULL")->result_array();
        
        return $query;
    }


    public function ubahStatus($id){
        $current_date = new Moment();
        $current_date = $current_date->format('Y-m-d');

        $query = $this->db->query("UPDATE tabel_reminder SET status = '$current_date' WHERE id = '$id'");
        return $query;
    }

    public function ubahMultiStatus($id){
        $current_date = new Moment();
        $current_date = $current_date->format('Y-m-d');

        $query = $this->db->query("UPDATE tabel_reminder SET status = '$current_date' WHERE id IN ($id)");
        return $query;
    }

    public function hapusReminderByPegawai($id){
        $query = $this->db->query("DELETE FROM tabel_reminder WHERE id_pegawai = '$id'");
    }
}