<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4">Reminder</h1>

            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table me-1"></i>
                    Data Reminder
                </div>
                <div class="card-body">
                    <div class="card">
                        <div class="card-body">
                            <div class="form-check">
                                <input class="form-check-input check-all-pengingat" type="checkbox" value="all" id="flexCheckDefault">
                                <label class="form-check-label" for="flexCheckDefault">
                                    Pilih semua
                                </label>

                                <button type="button" class="btn btn-sm btn-primary btn-multi-reminder mx-3"><i class="fab fa-fw fa-telegram"></i> Kirim</button>
                            </div>
                        </div>
                    </div>
                    <table id="datatablesSimple">
                        <thead>
                            <tr>
                                <th>Pilih</th>
                                <th>Nama</th>
                                <th>NIP</th>
                                <th>No. WA</th>
                                <th>TMT SK</th>
                                <th>Tgl. Pengingat</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php foreach ($reminders as $reminder) { ?>
                                <tr>
                                    <td class="center">
                                        <div class="form-check">
                                            <?php if($reminder['telegram_id'] != null ): ?>
                                            <input class="check-single-pengingat form-check-input" name="pilih[]" type="checkbox" value="<?= $reminder['id'] ?>" data-telegram_id="<?= $reminder['telegram_id'] ?>"id="flexCheckDefault">
                                            <?php endif ?>
                                        </div>
                                    </td>
                                    <td><?= $reminder['nama'] ?></td>
                                    <td><?= $reminder['nip'] ?></td>
                                    <td><?= $reminder['no_hp'] ?></td>
                                    <?php 
                                        $currentDate = new DateTime('now');
                                        $tmtSkDate = DateTime::createFromFormat('Y-m-d', $reminder['tmt_sk']);
                                        $interval = $currentDate->diff($tmtSkDate);
                                    ?>
                                    <td><?= $reminder['tmt_sk'] ?> (<?= $interval->days ?> hari lagi)</td>
                                    <td><?= $reminder['tanggal_pengingat'] ?> </td>
                                    <td>
                                        <button type="button" class="btn btn-sm btn-primary btn-reminder" data-reminder_id="<?= $reminder['id'] ?>" data-telegram_id="<?= $reminder['telegram_id'] ?>" <?= $reminder['telegram_id'] ? null : 'disabled' ?>><i class="fab fa-fw fa-telegram"></i></button>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </main>
    <footer class="py-4 bg-light mt-auto">
        <div class="container-fluid px-4">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; Your Website 2021</div>
                <div>
                    <a href="#">Privacy Policy</a>
                    &middot;
                    <a href="#">Terms &amp; Conditions</a>
                </div>
            </div>
        </div>
    </footer>
</div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="<?= base_url('assets/') ?>js/scripts.js"></script>
<script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
<script src="<?= base_url('assets/') ?>js/datatables-simple-demo.js"></script>

<!-- Modal Kirim Reminder -->
<div class="modal fade bs-example-modal" id="modalForm" data-toggle="modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="form" class="form-horizontal">
                <!-- <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Tambah Data</h4>
                </div> -->
                <div class="modal-body">
                    <input type="hidden" class="modal-reminder-id" name="telegram" value="">
                    <input type="hidden" class="modal-telegram-id" name="telegram" value="">
                    <div class="form-group">
                        <label>Isi Pesan</label>
                        <textarea name="pesan" id="input-pesan" rows="3" class="form-control" required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id='btn-batal' class="btn btn-default" data-dismiss="modal"><i class="fa fa-ban"></i> Batal</button>
                    <button type="button" id="btn-kirim" class="btn btn-success"><i class="fa fa-save"></i> Kirim</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade bs-example-modal" id="modalMultiPengingat" data-toggle="modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="form" class="form-horizontal">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Isi Pesan</label>
                        <textarea name="pesan" id="input-pesan-multi" rows="3" class="form-control" required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id='btn-multi-batal' class="btn btn-default" data-dismiss="modal"><i class="fa fa-ban"></i> Batal</button>
                    <button type="button" id="btn-multi-kirim" class="btn btn-success"><i class="fa fa-save"></i> Kirim</button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
<script type="text/javascript">
    function clear_form() {
        $('.modal-reminder-id').val('');
        $('.modal-telegram-id').val('');
        $('#input-kenaikan').val("default");
        $('#input-pesan').val('');
    }
    $('#btn-batal').on('click', function() {
        $('#modalForm').modal('hide')
    });

    $(function(){
      $(".btn-reminder").click(function(){

        clear_form();

        let reminder_id    = $(this).data('reminder_id');
        let telegram_id  = $(this).data('telegram_id');

        $(".modal-reminder-id").val(reminder_id);
        $(".modal-telegram-id").val(telegram_id);

        $("#modalForm").modal("show");
      });
    });

    $('#btn-kirim').on('click', function() {

        var reminder_id = $('.modal-reminder-id').val().trim();
        var telegram_id = $('.modal-telegram-id').val().trim();

        var pesan = $('#input-pesan').val();

        if(pesan.trim() == ''){
            alert('Pesan tidak boleh kosong');
        }else {
            $.ajax({
                url: "<?= base_url('telegram/kirimPengingat') ?>",
                type: "POST",
                dataType: "JSON",
                data: {
                    reminder_id: reminder_id,
                    telegram_id: telegram_id,
                    pesan: pesan
                },
                success: function(data) {
                    if (data.success == true) {
                        alert(data.messages);
                        location.reload();
                    } else {
                        alert(data.messages);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error');
                }
            });
        }
    });

    $(document).ready(function(){
        $('.check-all-pengingat').change(function(){
            if(this.checked){
                $('.check-single-pengingat').prop('checked', true);
                let telegram_id = $(".check-single-pengingat:checked").map(function () { return $(this).data('telegram_id'); }).get();
            }else{
                $('.check-single-pengingat').prop('checked', false);
            }
        });
        $('.btn-multi-reminder').click(function(){
            let reminder_id = $(".check-single-pengingat:checked").map(function () { return this.value; }).get();
            if(reminder_id === undefined || reminder_id.length == 0){
                alert("Silakan pilih pengingat yang akan dikirim");
            }else{
                $("#modalMultiPengingat").modal("show");
            }
        });
        $('#btn-multi-batal').click(function(){
            $("#modalMultiPengingat").modal("hide");
        });
        $('#btn-multi-kirim').click(function(){
            let reminder_id = $(".check-single-pengingat:checked").map(function () { return this.value; }).get();
            let telegram_id = $(".check-single-pengingat:checked").map(function () { return $(this).data('telegram_id'); }).get();
            var pesan = $('#input-pesan-multi').val();

            if(pesan.trim() == ''){
                alert('Pesan tidak boleh kosong');
            }else {
                $.ajax({
                    url: "<?= base_url('telegram/kirimMultiPengingat') ?>",
                    type: "POST",
                    dataType: "JSON",
                    data: {
                        reminder_id: reminder_id,
                        telegram_id: telegram_id,
                        pesan: pesan
                    },
                    success: function(data) {
                        if (data.success == true) {
                            alert(data.messages);
                            location.reload();
                        } else {
                            alert(data.messages);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR);
                        alert('Error');
                    }
                });
            }
        });
    });

</script>

</html>