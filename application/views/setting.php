<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4">Setting</h1>

            <div class="card mb-4">
                <div class="card-body p-5">
                    <div class="mb-3 row">
                        <label for="jangka_waktu" class="col-sm-2 col-form-label">Jarak Reminder dan Notifikasi</label>
                        <div class="col-sm-10">
                        <select class="form-select" id="jangka_waktu">
                            <option disabled>Pilih Jarak Reminder</option>
                            <option value="1" <?= ($jangka_waktu == 1) ? 'selected' : '' ?>>1 Bulan</option>
                            <option value="2" <?= ($jangka_waktu == 2) ? 'selected' : '' ?>>2 Bulan</option>
                            <option value="3" <?= ($jangka_waktu == 3) ? 'selected' : '' ?>>3 Bulan</option>
                            <option value="4" <?= ($jangka_waktu == 4) ? 'selected' : '' ?>>4 Bulan</option>
                            <option value="5" <?= ($jangka_waktu == 5) ? 'selected' : '' ?>>5 Bulan</option>
                            <option value="6" <?= ($jangka_waktu == 6) ? 'selected' : '' ?>>6 Bulan</option>
                            <option value="7" <?= ($jangka_waktu == 7) ? 'selected' : '' ?>>7 Bulan</option>
                            <option value="8" <?= ($jangka_waktu == 8) ? 'selected' : '' ?>>8 Bulan</option>
                            <option value="9" <?= ($jangka_waktu == 9) ? 'selected' : '' ?>>9 Bulan</option>
                            <option value="10 <?= ($jangka_waktu == 10) ? 'selected' : '' ?>">10 Bulan</option>
                            <option value="11 <?= ($jangka_waktu == 11) ? 'selected' : '' ?>">11 Bulan</option>
                            <option value="12 <?= ($jangka_waktu == 12) ? 'selected' : '' ?>">12 Bulan</option>
                        </select>
                        </div>
                    </div>
                    <div class="mb-3 row">
                        <label for="pesan" class="col-sm-2 col-form-label">Pesan</label>
                        <div class="col-sm-10">
                        <textarea class="form-control" id="pesan" rows=5><?=$pesan?></textarea>
                        <small><em>Kata <strong>"_jangka_waktu_"</strong> akan diganti dengan <strong>jarak notifikasi</strong> dan kata <strong>"_pesan_"</strong> akan diganti dengan <strong>tambahan pesan</strong> saat mengirim notifikasi.</em></small><br>
                        <small><em>Gunakan <strong>"_baris_baru_"</strong> untuk membuat baris baru (enter).</em></small>
                        </div>
                    </div>
                    <div class="row">
                        <label for="pesan" class="col-sm-2 col-form-label"></label>
                        <div class="col-sm-10">
                        <button type="button" id="btn-simpan" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <footer class="py-4 bg-light mt-auto">
        <div class="container-fluid px-4">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; Your Website 2021</div>
                <div>
                    <a href="#">Privacy Policy</a>
                    &middot;
                    <a href="#">Terms &amp; Conditions</a>
                </div>
            </div>
        </div>
    </footer>
</div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="<?= base_url('assets/') ?>js/scripts.js"></script>
<script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
<script src="<?= base_url('assets/') ?>js/datatables-simple-demo.js"></script>

<!-- Modal Tambah Data -->
<div class="modal fade bs-example-modal" id="modalForm" data-toggle="modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="form" class="form-horizontal">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Tambah Data</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group" id="idnya">
                        <label>ID</label>
                        <input name="id" id="id" type="text" class="form-control" readonly />
                    </div>
                    <div class="form-group">
                        <label>Nama</label>
                        <input name="nama" id="nama" type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>NIP</label>
                        <input name="nip" id="nip" type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Tempat Lahir</label>
                        <input name="tempat_lahir" id="tempat_lahir" type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Tanggal Lahir</label>
                        <input name="tgl_lahir" id="tgl_lahir" type="date" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input name="email" id="email" type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Bidang</label>
                        <input name="bidang" id="bidang" type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Nomor HP</label>
                        <input name="nomorhp" id="nomorhp" type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>TMT SK</label>
                        <input name="tmtsk" id="tmtsk" type="date" class="form-control" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id='btn-batal' class="btn btn-default" data-dismiss="modal"><i class="fa fa-ban"></i> Batal</button>
                    <button type="button" id="btn-simpan" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" id="btn-ubah" class="btn btn-info"><i class="fa fa-edit"></i> Ubah</button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
<script type="text/javascript">
    $(document).ready(function(){
        $('#btn-simpan').on('click', function() {
            let pesan = $('#pesan').val().trim();
            let jangka_waktu = $('#jangka_waktu').val().trim();

            if ((pesan == '') || (jangka_waktu == '')) {
                alert('Data tidak boleh kosong');
            } else {
                $.ajax({
                    type: "POST",
                    url: "<?= base_url('dashboard/simpansetting') ?>",
                    dataType: "JSON",
                    data: {
                        pesan: pesan,
                        jangka_waktu: jangka_waktu
                    },
                    success: function(data) {
                        if (data.success == true) {
                            alert(data.messages);
                            location.reload();
                        } else {
                            alert(data.messages);
                        }
                    }
                });
            }
        });
    });


</script>

</html>