<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4">Pegawai</h1>

            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table me-1"></i>
                    Data Pegawai
                </div>
                <div class="card-body">
                    <button class="btn btn-success" id="btn-tambah"><i class="fa fa-fw fa-plus"></i> Tambah Data</button>
                    <table id="datatablesSimple">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>NIP</th>
                                <th>Tempat Lahir</th>
                                <th>Tanggal Lahir</th>
                                <th>Email</th>
                                <th>Bidang</th>
                                <th>Nomor HP</th>
                                <th>TMT SK</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php foreach ($getallpegawai->result_array() as $data) { ?>
                                <tr>
                                    <td><?= $data['nama'] ?></td>
                                    <td><?= $data['nip'] ?></td>
                                    <td><?= $data['tempat_lahir'] ?></td>
                                    <td><?= $data['tanggal_lahir'] ?></td>
                                    <td><?= $data['email'] ?></td>
                                    <td>
                                        <?php 
                                        if($data['bidang'] == 'aplikasi_dan_informatika'){
                                            echo "Aplikasi dan Informatika";
                                        }else if($data['bidang'] == 'infrastruktur_tik'){
                                            echo "Infrastruktur TIK";
                                        }else if($data['bidang'] == 'persandian'){ 
                                            echo "Persandian";
                                        }else if($data['bidang'] == 'ikp'){
                                            echo "IKP";
                                        }else if($data['bidang'] == 'komisi_informasi_riau'){
                                            echo "Komisi Informasi Riau";
                                        }else{
                                            echo "Sekretariat Diskominfotik";
                                        }
                                        ?>
                                    </td>
                                    <td><?= $data['no_hp'] ?></td>
                                    <td><?= $data['tmt_sk'] ?></td>
                                    <td>
                                        <a onclick="hapusdata(`<?= $data['id'] ?>`)" class="btn btn-sm btn-danger"><i class="fa fa-fw fa-trash"></i></a>
                                        <a onclick="editdata(`<?= $data['id'] ?>`)" class="btn btn-sm btn-info" id=><i class="fa fa-fw fa-edit"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </main>
    <footer class="py-4 bg-light mt-auto">
        <div class="container-fluid px-4">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; Your Website 2021</div>
                <div>
                    <a href="#">Privacy Policy</a>
                    &middot;
                    <a href="#">Terms &amp; Conditions</a>
                </div>
            </div>
        </div>
    </footer>
</div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="<?= base_url('assets/') ?>js/scripts.js"></script>
<script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
<script src="<?= base_url('assets/') ?>js/datatables-simple-demo.js"></script>

<!-- Modal Tambah Data -->
<div class="modal fade bs-example-modal" id="modalForm" data-toggle="modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="form" class="form-horizontal">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus"></i> Tambah Data</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group" id="idnya">
                        <label>ID</label>
                        <input name="id" id="id" type="text" class="form-control" readonly />
                    </div>
                    <div class="form-group">
                        <label>Nama</label>
                        <input name="nama" id="nama" type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>NIP</label>
                        <input name="nip" id="nip" type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Tempat Lahir</label>
                        <input name="tempat_lahir" id="tempat_lahir" type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Tanggal Lahir (Format: <strong>BULAN/TANGGAL/TAHUN </strong>)</label>
                        <input name="tgl_lahir" id="tgl_lahir" type="date" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input name="email" id="email" type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Bidang</label>
                        <select class="form-select" name="bidang" id="bidang" aria-label="Default select example">
                            <option value="" disabled selected>Pilih bidang</option>
                            <option value="aplikasi_dan_informatika">Aplikasi dan Informatika</option>
                            <option value="infrastruktur_tik">Infrastruktur TIK</option>
                            <option value="persandian">Persandian</option>
                            <option value="ikp">IKP</option>
                            <option value="komisi_informasi_riau">Komisi Informasi Riau</option>
                            <option value="sekretariat_diskominfotik">Sekretariat Diskominfotik</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Nomor HP</label>
                        <input name="nomorhp" id="nomorhp" type="text" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>TMT SK  (Format: <strong>BULAN/TANGGAL/TAHUN </strong>)</label>
                        <input name="tmtsk" id="tmtsk" type="date" class="form-control" />
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id='btn-batal' class="btn btn-default" data-dismiss="modal"><i class="fa fa-ban"></i> Batal</button>
                    <button type="button" id="btn-simpan" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                    <button type="button" id="btn-ubah" class="btn btn-info"><i class="fa fa-edit"></i> Ubah</button>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
<script type="text/javascript">
    function clear_form() {
        $('#idnya').val('');
        $('#nama').val('');
        $('#nip').val('');
        $('#tempat_lahir').val('');
        $('#tgl_lahir').val('');
        $('#email').val('');
        // $('#bidang').val('');
        $('#nomorhp').val('');
        $('#tmtsk').val('');
    }
    $('#btn-batal').on('click', function() {
        $('#modalForm').modal('hide')
    });
    $('#btn-tambah').on('click', function() {
        $('#btn-simpan').show();
        $('#btn-ubah').hide();
        $('#idnya').hide();
        $('#modalForm').modal('show');
        $('#myModalLabel').html("<i class='fa fa-plus'></i> Tambah Data");
        clear_form();
    });
    $('#btn-simpan').on('click', function() {
        var nama = $('#nama').val().trim();
        var nip = $('#nip').val().trim();
        var tempat_lahir = $('#tempat_lahir').val().trim();
        var tgl_lahir = $('#tgl_lahir').val().trim();
        var email = $('#email').val().trim();
        var bidang = $('#bidang').val().trim();
        var nomorhp = $('#nomorhp').val().trim();
        var tmtsk = $('#tmtsk').val().trim();

        if ((nama == '') || (nip == '') || (tempat_lahir == '') || (tgl_lahir == '') || (email == '') || (bidang == '') || (nomorhp == '') || (tmtsk == '')) {
            alert('Data tidak boleh kosong');
        } else {
            $.ajax({
                type: "POST",
                url: "<?= base_url('dashboard/simpanpegawai') ?>",
                dataType: "JSON",
                data: {
                    nama: nama,
                    nip: nip,
                    tempat_lahir: tempat_lahir,
                    tgl_lahir: tgl_lahir,
                    email: email,
                    bidang: bidang,
                    nomorhp: nomorhp,
                    tmtsk: tmtsk,
                },
                success: function(data) {
                    if (data.success == true) {
                        alert(data.messages);
                        location.reload();
                    } else {
                        alert(data.messages);
                    }
                }
            });
        }
    });

    function hapusdata(id) {
        if (confirm('Apakah anda yakin ingin menghapus data ini?')) {
            $.ajax({
                url: "<?= base_url('dashboard/hapuspegawai') ?>",
                type: "POST",
                dataType: "JSON",
                data: {
                    id: id,
                },
                success: function(data) {
                    if (data.success == true) {
                        alert(data.messages);
                        location.reload();
                    } else {
                        alert(data.messages);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    alert('Error');
                }
            });
        }
    }

    function editdata(id) {
        $('#btn-ubah').show();
        $('#btn-simpan').hide();
        $('#idnya').hide();
        $('#myModalLabel').html("<i class='fa fa-edit'></i> Ubah Data");
        $.ajax({
            url: "<?= base_url('dashboard/ambilpegawai') ?>",
            type: "POST",
            dataType: "JSON",
            data: {
                id: id,
            },
            success: function(data) {
                if (data.success == true) {
                    $('#modalForm').modal('show');
                    $('#id').val(data.datanya.id);
                    $('#nama').val(data.datanya.nama);
                    $('#nip').val(data.datanya.nip);
                    $('#tempat_lahir').val(data.datanya.tempat_lahir);
                    $('#tgl_lahir').val(data.datanya.tanggal_lahir);
                    $('#email').val(data.datanya.email);
                    // $('#bidang').val(data.datanya.bidang);
                    $("#bidang").val(data.datanya.bidang).change();
                    $('#nomorhp').val(data.datanya.no_hp);
                    $('#tmtsk').val(data.datanya.tmt_sk);
                } else {
                    alert(data.messages);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error');
            }
        });
    }

    $('#btn-ubah').on('click', function() {
        $('#idnya').hide();
        var id = $('#id').val().trim();
        var nama = $('#nama').val().trim();
        var nip = $('#nip').val().trim();
        var tempat_lahir = $('#tempat_lahir').val().trim();
        var tgl_lahir = $('#tgl_lahir').val().trim();
        var email = $('#email').val().trim();
        var bidang = $('#bidang').val();
        var nomorhp = $('#nomorhp').val().trim();
        var tmtsk = $('#tmtsk').val().trim();

        if ((id == '') || (nama == '') || (nip == '') || (tempat_lahir == '') || (tgl_lahir == '') || (email == '') || (bidang == '') || (nomorhp == '') || (tmtsk == '')) {
            alert('Data tidak boleh kosong');
        } else {
            $.ajax({
                type: "POST",
                url: "<?= base_url('dashboard/ubahpegawai') ?>",
                dataType: "JSON",
                data: {
                    id: id,
                    nama: nama,
                    nip: nip,
                    tempat_lahir: tempat_lahir,
                    tgl_lahir: tgl_lahir,
                    email: email,
                    bidang: bidang,
                    nomorhp: nomorhp,
                    tmtsk: tmtsk,
                },
                success: function(data) {
                    if (data.success == true) {
                        alert(data.messages);
                        location.reload();
                    } else {
                        alert(data.messages);
                    }
                }
            });
        }
    });
</script>

</html>