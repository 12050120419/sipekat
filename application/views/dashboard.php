<div id="layoutSidenav_content">
    <main>
		<div class="container-fluid px-4">
            <h1 class="mt-4">Halaman Dashboard</h1>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="card col-5 mx-5 bg-primary">
                            <div class="row g-0">
                                <div class="col-md-2">
                                    <div class="card p-2 my-2 bg-primary">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="white" class="bi bi-person-fill" viewBox="0 0 16 16">
                                    <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3Zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6Z"/>
                                    </svg>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="card-body">
                                        <h3 class="card-title d-inline text-white">PEGAWAI: <?= $totalPegawai ?></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card col-5 mx-4 bg-success">
                            <div class="row g-0">
                                <div class="col-md-2">
                                    <div class="card p-2 my-2 bg-success">
                                        <svg xmlns="http://www.w3.org/2000/svg" fill="white" class="bi bi-briefcase-fill" viewBox="0 0 16 16">
                                        <path d="M6.5 1A1.5 1.5 0 0 0 5 2.5V3H1.5A1.5 1.5 0 0 0 0 4.5v1.384l7.614 2.03a1.5 1.5 0 0 0 .772 0L16 5.884V4.5A1.5 1.5 0 0 0 14.5 3H11v-.5A1.5 1.5 0 0 0 9.5 1h-3zm0 1h3a.5.5 0 0 1 .5.5V3H6v-.5a.5.5 0 0 1 .5-.5z"/>
                                        <path d="M0 12.5A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5V6.85L8.129 8.947a.5.5 0 0 1-.258 0L0 6.85v5.65z"/>
                                        </svg>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="card-body">
                                        <h3 class="card-title d-inline text-white">BIDANG: <?= $totalBidang ?></h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          	<img src="<?= base_url('assets/img/logo_dashboard.jpg') ?>" class="img-fluid img-thumbnail mt-2">
      	</div>
    </main>
    <footer class="py-4 bg-light mt-auto">
        <div class="container-fluid px-4">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; Your Website 2021</div>
                <div>
                    <a href="#">Privacy Policy</a>
                    &middot;
                    <a href="#">Terms &amp; Conditions</a>
                </div>
            </div>
        </div>
    </footer>
</div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="<?= base_url('assets/') ?>js/scripts.js"></script>
<script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
<script src="<?= base_url('assets/') ?>js/datatables-simple-demo.js"></script>
</body>

</html>