<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid px-4">
            <h1 class="mt-4">Status</h1>

            <div class="card mb-4">
                <div class="card-header">
                    <i class="fas fa-table me-1"></i>
                    Cek Status
                </div>
                <div class="card-body">
                    <!-- <button class="btn btn-success" id="btn-tambah"><i class="fa fa-fw fa-plus"></i> Tambah Reminder</button> -->
                    <table id="datatablesSimple">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Status</th>
                                <th>Tgl. Terkirim</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php foreach ($reminders as $reminder) { ?>
                                <tr>
                                    <td><?= $reminder['nama'] ?></td>
                                    <td><?= ($reminder['status'] == null) ? 'Belum Terkirim' : 'Terkirim' ?></td>
                                    <td><?= ($reminder['status'] == null) ? '&mdash;' : $reminder['status'] ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </main>
    <footer class="py-4 bg-light mt-auto">
        <div class="container-fluid px-4">
            <div class="d-flex align-items-center justify-content-between small">
                <div class="text-muted">Copyright &copy; Your Website 2021</div>
                <div>
                    <a href="#">Privacy Policy</a>
                    &middot;
                    <a href="#">Terms &amp; Conditions</a>
                </div>
            </div>
        </div>
    </footer>
</div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
<script src="<?= base_url('assets/') ?>js/scripts.js"></script>
<script src="https://cdn.jsdelivr.net/npm/simple-datatables@latest" crossorigin="anonymous"></script>
<script src="<?= base_url('assets/') ?>js/datatables-simple-demo.js"></script>
</div>
</body>
<script type="text/javascript">
    
</script>

</html>