<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Telegram extends CI_Controller
{
    private $apiUrl;
    function __construct()
	{
		parent::__construct();
		$this->load->database();
        $this->apiUrl = "https://api.telegram.org/bot" . $this->config->item('telegram_bot_token');
        $this->load->model('Model_Pegawai', 'model_pegawai');
        $this->load->model('Model_Reminder', 'model_reminder');
        $this->load->model('Model_Setting', 'model_setting');
	}

    public function listen(){
        $update = json_decode(file_get_contents("php://input"), TRUE);
        $chatId = $update["message"]["chat"]["id"];
        $message = $update["message"]["text"];

        $pegawai_telegram_id = $this->db->query("SELECT id FROM tabel_pegawai WHERE telegram_id = '$chatId'")->num_rows();
        
        if($message == '/start'){
            if($pegawai_telegram_id > 0){
                file_get_contents($this->apiUrl."/sendmessage?chat_id=".$chatId."&text=ID Telegram ini telah terdaftar!&parse_mode=HTML"); 
            }else{
                file_get_contents($this->apiUrl."/sendmessage?chat_id=".$chatId."&text=Silakan kirim NIP anda&parse_mode=HTML"); 
            }
        }else if($message != ''){
            $pegawai_nip = $this->db->query("SELECT id FROM tabel_pegawai WHERE nip = '$message'")->num_rows();
            if($pegawai_telegram_id > 0){
                file_get_contents($this->apiUrl."/sendmessage?chat_id=".$chatId."&text=ID Telegram ini telah terdaftar!&parse_mode=HTML"); 
            }else if($pegawai_nip == 0){
                file_get_contents($this->apiUrl."/sendmessage?chat_id=".$chatId."&text=Pastikan NIP anda telah terdaftar.&parse_mode=HTML");
            }else{
                $query = $this->model_pegawai->updateTelegramId($message, $chatId);
                if($query){
                    file_get_contents($this->apiUrl."/sendmessage?chat_id=".$chatId."&text=Telegram anda berhasil didaftarkan. Terimakasih.&parse_mode=HTML"); 
                }else{
                    file_get_contents($this->apiUrl."/sendmessage?chat_id=".$chatId."&text=Telegram anda gagal didaftarkan, silakan hubungi admin SIPEKAT.&parse_mode=HTML"); 
                }
            }
        }else{
            file_get_contents($this->apiUrl."/sendmessage?chat_id=".$chatId."&text=Pesan tidak boleh kosong!.&parse_mode=HTML"); 
        }
        
    }

    public function setWebHook(){
        
        file_get_contents($this->apiUrl . "/setWebhook?url=" . $this->config->item('base_url') . "telegram/listen"); 
        return "ok";
    }

    public function kirimPengingat(){
        $reminder_id = trim($this->input->post('reminder_id'));
        $telegram_id = trim($this->input->post('telegram_id'));
        $pesan = trim($this->input->post('pesan'));

        $pesanDefault = $this->model_setting->getPesan();
        $jangkaWaktu = $this->model_setting->getJangkaWaktu();

        $finalPesan = str_replace('_jangka_waktu_', $jangkaWaktu, $pesanDefault);
        $finalPesan = str_replace('_pesan_', $pesan, $finalPesan);
        $finalPesan = str_replace('_baris_baru_', '%0A', $finalPesan);

        file_get_contents($this->apiUrl."/sendmessage?chat_id=".$telegram_id."&text=".$finalPesan."&parse_mode=HTML"); 

        $ubahStatus = $this->model_reminder->ubahStatus($reminder_id);

		if ($ubahStatus) {
			$datajson = array(
				'success' => true,
				'messages' => 'Status pengingat: telah terkirim',
			);
		} else {
			$datajson = array(
				'success' => false,
				'messages' => 'Status gagal diubah',
			);
		}
		echo json_encode($datajson);
    }

    public function kirimMultiPengingat(){
        $reminder_ids = $string = implode(', ', $this->input->post('reminder_id'));
        $telegram_ids = $this->input->post('telegram_id');
        $pesan = trim($this->input->post('pesan'));

        $pesanDefault = $this->model_setting->getPesan();
        $jangkaWaktu = $this->model_setting->getJangkaWaktu();

        $finalPesan = str_replace('_jangka_waktu_', $jangkaWaktu, $pesanDefault);
        $finalPesan = str_replace('_pesan_', $pesan, $finalPesan);
        $finalPesan = str_replace('_baris_baru_', '%0A', $finalPesan);

        foreach ($telegram_ids as $key => $telegram_id) {
            file_get_contents($this->apiUrl."/sendmessage?chat_id=".$telegram_id."&text=".$finalPesan."&parse_mode=HTML"); 
        }
        $ubahStatus = $this->model_reminder->ubahMultiStatus($reminder_ids);

		if ($ubahStatus) {
			$datajson = array(
				'success' => true,
				'messages' => 'Status pengingat: telah terkirim',
			);
		} else {
			$datajson = array(
				'success' => false,
				'messages' => 'Status gagal diubah',
			);
		}
		echo json_encode($datajson);
    }
}