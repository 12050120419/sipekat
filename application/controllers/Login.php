<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('sess_id')) {
            redirect(base_url('dashboard'));
        }

        $this->load->model('Model_Login');
    }

    public function index()
    {
        $this->load->view('login');
    }

    public function proses()
    {
        $this->form_validation->set_rules('username', 'Username', 'required|trim|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'required|trim|xss_clean');
        if ($this->form_validation->run() == FALSE) {
            $this->load->view('login');
        } else {
            echo $usr = $this->input->post('username');
            echo $psw = $this->input->post('password');
            $u = $usr;
            $p = md5($psw);
            $cek = $this->Model_Login->cek($u, $p);
            if ($cek->num_rows() > 0) {
                $qad = $cek->row();
                $sess_data['sess_id'] = $qad->id;
                $sess_data['sess_username'] = $qad->username;
                $sess_data['sess_nama'] = $qad->nama;
                $this->session->set_userdata($sess_data);
                $this->session->set_flashdata('success', 'Login Berhasil !');
                redirect(base_url('dashboard'));
            } else {
                $this->session->set_flashdata('result_login', 'Username atau Password yang anda masukkan salah.');
                redirect(base_url('login'));
            }
        }
    }
}
