<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		// if (!isset($this->session->userdata['sess_id'])) {
		// 	redirect(base_url("login"));
		// }
		$this->load->model('Model_Pegawai', 'model_pegawai');
		$this->load->model('Model_Reminder', 'model_reminder');
		$this->load->model('Model_Setting', 'model_setting');
		
        $this->load->database();
	}

	public function tes(){
		print_r($this->db->query("SELECT * FROM tabel_reminder WHERE `id_pegawai` = 29 AND `status` IS NULL ORDER BY id DESC LIMIT 1")->row());
	}

	public function index()
	{
		$countPegawai = $this->model_pegawai->countPegawai();
		$countBidang = $this->model_pegawai->countBidang();
		$data = [
			'totalPegawai' => $countPegawai,
			'totalBidang' => $countBidang,
		];
		$this->load->view('topbar');
		$this->load->view('dashboard', $data);
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url("login"));
	}

	public function pegawai()
	{
		$data = array(
			'getallpegawai' => $this->model_pegawai->getAllPegawai(),

		);
		$this->load->view('topbar');
		$this->load->view('pegawai', $data);
	}
	public function simpanpegawai()
	{
		$nama = trim($this->input->post('nama'));
		$nip = trim($this->input->post('nip'));
		$tempat_lahir = trim($this->input->post('tempat_lahir'));
		$tgl_lahir = trim($this->input->post('tgl_lahir'));
		$email = trim($this->input->post('email'));
		$bidang = trim($this->input->post('bidang'));
		$nomorhp = trim($this->input->post('nomorhp'));
		$tmtsk = trim($this->input->post('tmtsk'));

		$cekData = $this->model_pegawai->ambilpegawai_nip($nip);
		if ($cekData->num_rows() > 0) {
			$datajson = array(
				'success' => false,
				'messages' => 'NIP tersebut sudah ada',
			);
		} else {
			$simpanData = $this->model_pegawai->simpanpegawai($nip, $nama, $tempat_lahir, $tgl_lahir, $email, $bidang, $nomorhp, $tmtsk);
			if ($simpanData) {
				$datajson = array(
					'success' => true,
					'messages' => 'Data berhasil tersimpan',
				);
			} else {
				$datajson = array(
					'success' => false,
					'messages' => 'Data gagal tersimpan',
				);
			}
		}

		echo json_encode($datajson);
	}

	public function hapuspegawai()
	{
		$id = trim($this->input->post('id'));
		$hapusData = $this->model_pegawai->hapusPegawai($id);
		if ($hapusData) {
			$this->model_reminder->hapusReminderByPegawai($id);
			$datajson = array(
				'success' => true,
				'messages' => 'Data berhasil terhapus',
			);
		} else {
			$datajson = array(
				'success' => false,
				'messages' => 'Data gagal terhapus',
			);
		}
		echo json_encode($datajson);
	}

	public function ambilpegawai()
	{
		$id = trim($this->input->post('id'));
		$ambilData = $this->model_pegawai->ambilpegawai($id);
		if ($ambilData->num_rows() > 0) {
			$datajson = array(
				'success' => true,
				'messages' => 'Data berhasil diambil',
				'datanya' => $ambilData->row(),
			);
		} else {
			$datajson = array(
				'success' => false,
				'messages' => 'Data gagal diambil',
				'datanya' => ''
			);
		}
		echo json_encode($datajson);
	}

	public function ubahpegawai()
	{
		$id = trim($this->input->post('id'));
		$nama = trim($this->input->post('nama'));
		$nip = trim($this->input->post('nip'));
		$tempat_lahir = trim($this->input->post('tempat_lahir'));
		$tgl_lahir = trim($this->input->post('tgl_lahir'));
		$email = trim($this->input->post('email'));
		$bidang = trim($this->input->post('bidang'));
		$nomorhp = trim($this->input->post('nomorhp'));
		$tmtsk = trim($this->input->post('tmtsk'));

		$cekIDnya = $this->model_pegawai->ambilpegawai($id);
		if ($cekIDnya->num_rows() > 0) {
			$ubahData = $this->model_pegawai->ubahpegawai($id, $nip, $nama, $tempat_lahir, $tgl_lahir,$email, $bidang, $nomorhp, $tmtsk);
			$ubahData = true;
			if ($ubahData) {
				$datajson = array(
					'success' => true,
					'messages' => 'Data berhasil diubah'
				);
			} else {
				$datajson = array(
					'success' => false,
					'messages' => 'Data gagal diubah',
				);
			}
		} else {
			$datajson = array(
				'success' => false,
				'messages' => 'Gagal update data',
			);
		}
		echo json_encode($datajson);
	}
	
	
	public function realtime()
	{
		//$this->load->view('topbar');
		//$this->load->view('dashboard');
		 // Konfigurasi email
        $config = [
            'mailtype'  => 'html',
            'charset'   => 'utf-8',
            'protocol'  => 'smtp',
            'smtp_host' => 'smtp.gmail.com',
            'smtp_user' => 'novendra.anggi@gmail.com',  // Email gmail
            'smtp_pass'   => 'Bassistz13',  // Password gmail
            'smtp_crypto' => 'ssl',
            'smtp_port'   => 465,
            'crlf'    => "\r\n",
            'newline' => "\r\n"
        ];

        // Load library email dan konfigurasinya
        $this->load->library('email', $config);

        // Email dan nama pengirim
        $this->email->from('novendra.anggi@gmail.com', 'gmail.com');

        // Email penerima
        $this->email->to('syamrizal.anggga@gmail.com'); // Ganti dengan email tujuan

        // Lampiran email, isi dengan url/path file
      //  $this->email->attach('https://images.pexels.com/photos/169573/pexels-photo-169573.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940');

        // Subject email
        $this->email->subject('Kirim Email dengan SMTP Gmail CodeIgniter | MasRud.com');

        // Isi email
        $this->email->message("Ini adalah contoh email yang dikirim menggunakan SMTP Gmail pada CodeIgniter.<br><br> Klik <strong><a href='https://masrud.com/kirim-email-smtp-gmail-codeigniter/' target='_blank' rel='noopener'>disini</a></strong> untuk melihat tutorialnya.");

        // Tampilkan pesan sukses atau error
        if ($this->email->send()) {
            echo 'Sukses! email berhasil dikirim.';
        } else {
            echo 'Error! email tidak dapat dikirim.';
        }
	}

	//MENU REMINDER
	public function reminder(){
		$data = array(
			'reminders' => $this->model_reminder->getActiveReminders(),
		);

		$this->load->view('topbar');
		$this->load->view('reminder', $data);
	}

	public function ubahstatusreminder()
	{
		$id = trim($this->input->post('id'));
		$ubahStatus = $this->model_reminder->ubahStatus($id);
		if ($ubahStatus) {
			$datajson = array(
				'success' => true,
				'messages' => 'Status berhasil diubah',
			);
		} else {
			$datajson = array(
				'success' => false,
				'messages' => 'Status gagal diubah',
			);
		}
		echo json_encode($datajson);
	}



	//MENU STATUS
	public function status(){
		$data = array(
			'reminders' => $this->model_reminder->getAllReminders(),
		);
		$this->load->view('topbar');
		$this->load->view('status', $data);
		
	}

	//MENU SETTING
	public function setting(){
		$data = array(
			'pesan' => $this->model_setting->getPesan(),
			'jangka_waktu' => $this->model_setting->getJangkaWaktu(),
		);
		$this->load->view('topbar');
		$this->load->view('setting', $data);
		
	}

	public function simpansetting()
	{
		$pesan = trim($this->input->post('pesan'));
		$jangka_waktu = trim($this->input->post('jangka_waktu'));

		$setting = $this->model_setting->updateSetting($pesan, $jangka_waktu);
		if ($setting) {
			$datajson = array(
				'success' => true,
				'messages' => 'Setting telah diubah.',
			);
		} else {
			$datajson = array(
				'success' => false,
				'messages' => 'Setting gagal diubah',
			);
		}
		echo json_encode($datajson);
	}

}
