-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.33 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for sipekat
CREATE DATABASE IF NOT EXISTS `sipekat` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `sipekat`;

-- Dumping structure for table sipekat.tabel_pegawai
CREATE TABLE IF NOT EXISTS `tabel_pegawai` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(20) NOT NULL,
  `nama` text NOT NULL,
  `tempat_lahir` text NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `email` varchar(50) NOT NULL,
  `bidang` varchar(100) DEFAULT NULL,
  `no_hp` varchar(15) NOT NULL,
  `tmt_sk` date NOT NULL,
  `telegram_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table sipekat.tabel_pegawai: ~3 rows (approximately)
/*!40000 ALTER TABLE `tabel_pegawai` DISABLE KEYS */;
INSERT INTO `tabel_pegawai` (`id`, `nip`, `nama`, `tempat_lahir`, `tanggal_lahir`, `email`, `bidang`, `no_hp`, `tmt_sk`, `telegram_id`) VALUES
	(1, '199111132020121007', 'ANGGI NOVENDRA. ST', 'Ujung Batu', '2000-01-01', 'anggi.novendra@gmail.com', 'HR', '0821711051131', '2020-12-01', NULL),
	(23, '100000', 'Tes reminder data', 'tes', '1999-10-10', 'email@dsa', 'IT', '082300000000', '2023-06-10', NULL),
	(24, '09888', 'tes 2', 'tes 2', '2000-12-11', 'tes2@s', 'IT', '082300000001', '2023-06-12', NULL);
/*!40000 ALTER TABLE `tabel_pegawai` ENABLE KEYS */;

-- Dumping structure for table sipekat.tabel_reminder
CREATE TABLE IF NOT EXISTS `tabel_reminder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_pegawai` int(11) DEFAULT NULL,
  `tanggal_pengingat` date DEFAULT NULL,
  `status` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table sipekat.tabel_reminder: ~6 rows (approximately)
/*!40000 ALTER TABLE `tabel_reminder` DISABLE KEYS */;
INSERT INTO `tabel_reminder` (`id`, `id_pegawai`, `tanggal_pengingat`, `status`) VALUES
	(5, 23, '2022-11-15', NULL),
	(6, 24, '2023-03-12', '2023-03-24'),
	(10, 23, '2023-04-15', NULL),
	(11, 23, '2023-04-05', NULL),
	(12, 23, '2023-02-05', '2023-03-24'),
	(13, 23, '2023-01-28', NULL),
	(14, 23, '2023-03-10', NULL);
/*!40000 ALTER TABLE `tabel_reminder` ENABLE KEYS */;

-- Dumping structure for table sipekat.tabel_setting
CREATE TABLE IF NOT EXISTS `tabel_setting` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pesan` varchar(255) DEFAULT 'Anda akan naik pangkat dalam _jangka_waktu_ bulan. _baris_baru__pesan_.',
  `jangka_waktu` enum('1','2','3','4','5','6','7','8','9','10','11','12') NOT NULL DEFAULT '3',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table sipekat.tabel_setting: ~0 rows (approximately)
/*!40000 ALTER TABLE `tabel_setting` DISABLE KEYS */;
INSERT INTO `tabel_setting` (`id`, `pesan`, `jangka_waktu`) VALUES
	(1, 'Anda akan naik pangkat dalam _jangka_waktu_ bulan. _baris_baru__pesan_. _baris_baru_Selamat!!!.', '3');
/*!40000 ALTER TABLE `tabel_setting` ENABLE KEYS */;

-- Dumping structure for table sipekat.tabel_user
CREATE TABLE IF NOT EXISTS `tabel_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` text,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(256) DEFAULT NULL,
  `email` varchar(256) DEFAULT NULL,
  `no_hp` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table sipekat.tabel_user: ~0 rows (approximately)
/*!40000 ALTER TABLE `tabel_user` DISABLE KEYS */;
INSERT INTO `tabel_user` (`id`, `nama`, `username`, `password`, `email`, `no_hp`) VALUES
	(3, 'Administrator', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@gmail.com', '082171105113');
/*!40000 ALTER TABLE `tabel_user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
